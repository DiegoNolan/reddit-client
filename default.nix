{ mkDerivation, aeson, base, classy-prelude, exceptions
, http-client, http-client-tls, http-types, HUnit, monad-control
, split, stdenv, test-framework, test-framework-hunit, time
, transformers, transformers-base, vector
}:
mkDerivation {
  pname = "reddit-client";
  version = "0.1.0.0";
  src = ./.;
  libraryHaskellDepends = [
    aeson base classy-prelude exceptions http-client http-client-tls
    http-types monad-control split time transformers transformers-base
    vector
  ];
  testHaskellDepends = [
    aeson base classy-prelude http-client http-client-tls http-types
    HUnit split test-framework test-framework-hunit time vector
  ];
  license = stdenv.lib.licenses.mit;
}
