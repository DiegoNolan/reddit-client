{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import ClassyPrelude
import Data.Aeson
import Test.HUnit
import Test.Framework (defaultMain)
import Test.Framework.Providers.HUnit (hUnitTestToTests)
import Reddit.CommentTree
import qualified Reddit.Comment as C
import qualified Reddit.Link as L
import qualified Reddit.Subreddit as S

assertRight :: Either String a -> Test
assertRight (Right _) = TestCase $ assertString ""
assertRight (Left s) = TestCase $ assertString s

main :: IO ()
main = do
    subreddits <- readFile "tst/json/subreddits.json"
    links <- readFile "tst/json/links.json"
    commentTree <- readFile "tst/json/comment-tree.json"
    moreComments <- readFile "tst/json/more-comments.json"
    defaultMain
        (hUnitTestToTests $ TestLabel "Json parsing" (TestList
                [ testSubreddits subreddits
                , testLinks links
                , testCommentTree commentTree
                , testMoreComments moreComments
                ])
        )

testSubreddits :: ByteString -> Test
testSubreddits contents = TestLabel "Subreddits" $
    assertRight (eitherDecode (fromStrict contents) :: Either String S.Subreddits)

testLinks :: ByteString -> Test
testLinks contents = TestLabel "Links" $
    assertRight (eitherDecode (fromStrict contents) :: Either String L.Links)

testCommentTree :: ByteString -> Test
testCommentTree contents = TestLabel "CommentTree" $
    assertRight (eitherDecode (fromStrict contents) :: Either String CommentTree)

testMoreComments :: ByteString -> Test
testMoreComments contents = TestLabel "Comments" $
    assertRight (eitherDecode (fromStrict contents) :: Either String C.MoreComments)

