{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Reddit.Utility
  ( parseRedditTime
  ) where

import ClassyPrelude
import Data.Aeson.Types (Parser)

parseRedditTime :: Float -> Parser UTCTime
parseRedditTime f =
  case parseTimeM True defaultTimeLocale "%s" (show (floor f :: Integer)) of
    Nothing -> mzero
    Just t  -> return t

