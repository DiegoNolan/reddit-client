{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Reddit.Subreddit
  ( Subreddit (..)
  , Subreddits (..)
  ) where

import ClassyPrelude
import Data.Aeson

import Reddit.Utility (parseRedditTime)

newtype Subreddits = Subreddits { toSubreddits :: [Subreddit] } deriving Show

instance FromJSON Subreddits where
    parseJSON (Object top) = do
        o <- top .: "data"
        Subreddits <$> o .: "children"
    parseJSON _ = mzero

data Subreddit = Subreddit
  { created_utc       :: !UTCTime
  , description       :: !Text
  , display_name      :: !Text
  , id                :: !Text
  , name              :: !Text
  , over18            :: !Bool
  , public_traffic    :: !Bool
  , submission_type   :: !Text
  , submit_text       :: !Text
  , subreddit_type    :: !Text
  , subscribers       :: !Int
  , title             :: !Text
  , url               :: !Text
  } deriving Show

instance FromJSON Subreddit where
    parseJSON (Object wrapper) = do
        o <- wrapper .: "data"
        Subreddit
            <$> (o .: "created_utc" >>= parseRedditTime)
            <*> o .: "description"
            <*> o .: "display_name"
            <*> o .: "id"
            <*> o .: "name"
            <*> o .: "over18"
            <*> o .: "public_traffic"
            <*> o .: "submission_type"
            <*> o .: "submit_text"
            <*> o .: "subreddit_type"
            <*> o .: "subscribers"
            <*> o .: "title"
            <*> o .: "url"
    parseJSON _ = mzero
    

