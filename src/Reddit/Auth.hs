{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module Reddit.Auth
  ( AuthInfo (..)
  , AuthToken (..)
  , authFromFile
  , getToken
  , refreshToken
  ) where

import ClassyPrelude
import Data.Aeson
import Network.HTTP.Client
import Network.HTTP.Client.TLS

data AuthInfo = AuthInfo
  { username      :: !ByteString
  , password      :: !ByteString
  , clientId      :: !ByteString
  , clientSecret  :: !ByteString
  } deriving Show

data AuthToken = AuthToken
  { access_token  :: !Text
  , expires_in    :: !Int
  , scope         :: !Text
  , token_type    :: !Text
  } deriving Show

instance FromJSON AuthToken where
  parseJSON (Object o) =
    AuthToken
      <$> o .: "access_token"
      <*> o .: "expires_in"
      <*> o .: "scope"
      <*> o .: "token_type"
  parseJSON _ = mzero

-- | Load the users auth from a file with a record on each line
authFromFile :: FilePath -> IO (Either String AuthInfo)
authFromFile fp = do
  lns <- lines <$> readFileUtf8 fp
  case map encodeUtf8 lns of
    [u, p, id_, key] -> return $ Right (AuthInfo u p id_ key)
    _ -> return $ Left "Auth file not in the correct format"

-- | Request a oauth token from reddit
getToken :: AuthInfo -> IO (Either String AuthToken)
getToken AuthInfo{..} = do
  putStrLn "Getting token"
  case parseUrlThrow "https://ssl.reddit.com/api/v1/access_token" of
    Nothing -> return $ Left "Could not parse Url"
    Just rq -> do
      manager <- newManager tlsManagerSettings
      let modifiedReq =
                    applyBasicAuth clientId clientSecret $
                    urlEncodedBody
                        [ ("grant_type", "password")
                        , ("username", username)
                        , ("password", password)
                        ]
                    rq
      resp <- httpLbs modifiedReq manager
      print (responseBody resp)
      let eRes = eitherDecode (responseBody resp)
      print eRes
      return eRes

refreshToken :: AuthInfo -> AuthToken -> IO (Either String AuthToken)
refreshToken AuthInfo{..} AuthToken{..} = do
  putStrLn "refreshing token"
  case parseUrlThrow "https://ssl.reddit.com/api/v1/access_token" of
    Nothing -> return $ Left "Could not parse Url"
    Just rq -> do
      manager <- newManager tlsManagerSettings
      let modifiedReq =
                    applyBasicAuth clientId clientSecret $
                    urlEncodedBody
                        [ ("grant_type", "refresh_token")
                        , ("refresh_token" , encodeUtf8 access_token)
                        ]
                    rq
      resp <- httpLbs modifiedReq manager
      let eRes = eitherDecode (responseBody resp)
      print eRes
      return eRes

