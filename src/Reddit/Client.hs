{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Reddit.Client
  ( Sort
  , confidence
  , top
  , new
  , hot
  , controversial
  , old
  , random
  , getSubreddit
  , getSubreddits
  , getLinks
  , getCommentTree
  , getMoreChildren
  , module Reddit.Auth
  ) where

import           ClassyPrelude
import           Control.Monad.Catch
import           Data.Aeson
import           Network.HTTP.Client
import           Network.HTTP.Types.Header
import           Reddit.Auth
import qualified Reddit.Comment            as C
import           Reddit.CommentTree
import qualified Reddit.Link               as L
import qualified Reddit.Subreddit          as S

chunksOf :: Int -> [a] -> [[a]]
chunksOf _ [] = []
chunksOf sz lst = f : chunksOf sz rest
    where (f, rest) = splitAt sz lst

type Sort = ByteString

confidence :: Sort
confidence = "confidence"

top :: Sort
top = "top"

new :: Sort
new = "new"

hot :: Sort
hot = "hot"

controversial :: Sort
controversial = "controversial"

old :: Sort
old = "old"

random :: Sort
random = "random"

baseDomain :: String
baseDomain = "https://reddit.com"

authDomain :: String
authDomain = "https://oauth.reddit.com"

getSubreddit :: Manager
             -> Maybe AuthToken
             -> ByteString
             -> Text
             -> IO (Either String S.Subreddit)
getSubreddit manager mToken userAgent subName = do
  rq <- mkModdedRq mToken userAgent ("/r/" ++ unpack subName ++ "/about.json")
  response <- httpLbs rq manager
  return $ eitherDecode (responseBody response)

getSubreddits :: Manager
              -> Maybe AuthToken
              -> ByteString -- ^ User Agent
              -> String
              -> Maybe Int
              -> Maybe Text
              -> Maybe Text
              -> IO (Either String [S.Subreddit])
getSubreddits manager mToken userAgent sorting limit before after = do
    rq <- mkModdedRq mToken userAgent ("/subreddits/" ++ sorting ++ ".json")
    let modifiedReq =
            setQueryString
                (filter (isJust . snd)
                [ ("limit", encodeUtf8 . tshow <$> limit)
                , ("before", encodeUtf8 <$> before)
                , ("after", encodeUtf8 <$> after)
                ]) rq
    response <- httpLbs modifiedReq manager
    return $ S.toSubreddits <$> eitherDecode (responseBody response)

getLinks :: Manager
         -> Maybe AuthToken
         -> ByteString  -- ^ User Agent
         -> Text        -- ^ Subreddit to crawl
         -> Sort        -- ^ How to sort the results
         -> Maybe Int   -- ^ Number of links to return, default 25, max 100
         -> IO (Either String [L.Link])
getLinks manager mToken userAgent subreddit sorting limit = do
    rq <- mkModdedRq mToken userAgent ("/r/" ++ unpack subreddit ++ "/hot.json")
    let modifiedReq =
            setQueryString
                ( ("sort", Just sorting) :
                case limit of
                    Nothing -> []
                    Just lim -> [ ( "limit", Just $ encodeUtf8 (tshow lim) ) ]
                ) rq
    response <- httpLbs modifiedReq manager
    return $ L.toLinks <$> eitherDecode (responseBody response)

getCommentTree :: Manager
               -> Maybe AuthToken
               -> ByteString  -- ^ User Agent
               -> Text        -- ^ Subreddit to crawl
               -> Sort        -- ^ How to sort the results
               -> Maybe Int   -- ^ Limit of comment depth to return, default 25, max 100
               -> IO (Either String CommentTree)
getCommentTree manager mToken userAgent article sorting limit = do
    rq <- mkModdedRq mToken userAgent ("/comments/" ++ unpack article ++ ".json")
    let modifiedReq =
            setQueryString
                ( ("sort", Just sorting) :
                case limit of
                    Nothing -> []
                    Just lim -> [ ( "limit", Just $ encodeUtf8 (tshow lim) ) ]
                ) rq
    response <- httpLbs modifiedReq manager
    return $ eitherDecode (responseBody response)

getMoreChildren :: Manager
                -> Maybe AuthToken
                -> ByteString -- ^ User Agent
                -> Text       -- ^ link id
                -> [Text]     -- ^ Full name of children to get
                -> IO (Either String [C.Comment])
getMoreChildren manager mToken userAgent linkId allChildren =
  fmap (fmap concat . sequence) $
    forM (chunksOf 20 allChildren) $ \children -> do
      rq <- mkModdedRq mToken userAgent "/api/morechildren.json"
      let modReq = urlEncodedBody
                        [ ("api_type", "json")
                        , ("link_id", encodeUtf8 linkId)
                        , ("children", encodeUtf8 . intercalate "," $ children)
                        ] rq
      response <- httpLbs modReq manager
      return  $ C.toComments <$> eitherDecode (responseBody response)


getMe :: Manager
      -> AuthToken
      -> ByteString
      -> IO ()
getMe manager token userAgent = do
  rq <- mkModdedRq (Just token) userAgent "/api/v1/me"
  response <- httpLbs rq manager
  print (responseBody response)
  return ()


mkModdedRq :: MonadThrow m
           => Maybe AuthToken
           -> ByteString
           -> String
           -> m Request
mkModdedRq mToken userAgent path = do
  rq <- parseRequest $ case mToken of
                         Nothing -> baseDomain ++ path
                         Just _  -> authDomain ++ path
  let modReq = rq { requestHeaders =
                        (case mToken of
                           Nothing -> []
                           Just AuthToken{..} ->
                                [ ("Authorization"
                                    , encodeUtf8 $ token_type ++ " " ++ access_token
                                    )
                                ]
                        ) ++ requestHeaders rq
                   }
  return $ setUserAgent userAgent modReq

setUserAgent :: ByteString -> Request -> Request
setUserAgent userAgent rq =
  rq { requestHeaders = (hUserAgent, userAgent) : requestHeaders rq }

