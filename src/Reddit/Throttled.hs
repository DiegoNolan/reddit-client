{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE UndecidableInstances      #-}
module Reddit.Throttled
  ( Reddit
  , RedditT (..)
  , MonadReddit (..)
  , redditTime
  , startReddit
  , printTPS
  , Sort
  , confidence
  , top
  , new
  , hot
  , controversial
  , old
  , random
  , getSubreddit
  , getSubreddits
  , getLinks
  , getCommentTree
  , getMoreChildren
  ) where

import           ClassyPrelude               hiding (catch, throwM)
import           Control.Concurrent
import           Control.Monad.Base
import           Control.Monad.Catch
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Control
import           Data.Time.Clock
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Reddit.Client               (Sort, confidence, controversial,
                                              hot, new, old, random, top)
import qualified Reddit.Client               as C
import qualified Reddit.Comment              as C
import           Reddit.CommentTree
import qualified Reddit.Link                 as L
import qualified Reddit.Subreddit            as S

data Throttle = Throttle
    { startTime   :: !UTCTime     -- ^ We started maying requests
    , numRequests :: !Integer     -- ^ Counter of the requests
    , tps         :: !Rational    -- ^ Transactions per second
    , userAgent   :: !ByteString
    , manager     :: !Manager
    , authInfo    :: !(Maybe (C.AuthInfo, C.AuthToken, UTCTime))
    }

type Reddit = RedditT IO

newtype RedditT m a = RedditT
   { runReddit :: Throttle -> m (a, Throttle)
   }

class (MonadIO m, MonadCatch m) => MonadReddit m where
  getTPS :: m Rational
  getUserAgent :: m ByteString
  numTrans :: Integer -> IO a -> m a
  getManager :: m Manager
  getToken :: m (Maybe C.AuthToken)

instance (MonadIO m, MonadCatch m) => MonadReddit (RedditT m) where
  getTPS = RedditT $ \t -> do
            now <- liftIO getCurrentTime
            let count = toRational (numRequests t)
                timeElasped = toRational (diffUTCTime now (startTime t))
            return (count / timeElasped , t)
  getUserAgent = RedditT $ \t -> return (userAgent t, t)
  numTrans d m =
    RedditT $ \(Throttle s n r ua man ai) -> do
        a <- liftIO m
        return (a, Throttle s (n+d) r ua man ai)
  getManager = RedditT $ \t -> return (manager t, t)
  getToken = RedditT $ \throttle ->
    case authInfo throttle of
      Nothing -> return (Nothing, throttle)
      Just aInfo -> do
        newAuthInfo@(_, at, _) <- liftIO $ getUpdatedAuthInfo aInfo
        return ( Just at
               , throttle { authInfo = Just newAuthInfo }
               )

getUpdatedAuthInfo :: (C.AuthInfo, C.AuthToken, UTCTime)
                   -> IO (C.AuthInfo, C.AuthToken, UTCTime)
getUpdatedAuthInfo (ai, at, t) = do
  now <- getCurrentTime
  let diffTime = diffUTCTime t now
      myGetToken = do
         eAT <- C.getToken ai
         case eAT of
           -- TODO: make better
           Left er -> print er >> error "Could not refresh token"
           Right at' -> return ( ai
                               , at'
                               , addUTCTime (fromIntegral $ C.expires_in at' - 5 * 60)
                                            now
                               )
  if | diffTime < 5 * 60 -> do
         eAT <- C.refreshToken ai at
         case eAT of
           -- TODO: make better
           Left er -> myGetToken
           Right at' -> return ( ai
                               , at'
                               , addUTCTime (fromIntegral $ C.expires_in at' - 5 * 60)
                                            now
                               )
     | diffTime < 0 -> myGetToken
     | otherwise -> return (ai, at, t)

startReddit :: MonadIO m
            => Maybe C.AuthInfo
            -> ByteString
            -> Rational
            -> RedditT m a -> m a
startReddit mAuthInfo ua rate reddit = do
  now <- liftIO getCurrentTime
  m <- liftIO $ newManager tlsManagerSettings
  authInfo <- case mAuthInfo of
                Nothing -> return Nothing
                Just aInfo -> do
                  eToken <- liftIO $ C.getToken aInfo
                  case eToken of
                    -- TODO: handle this better
                    Left er -> print er >> error "Failed authenticating"
                    Right token -> return $
                      Just ( aInfo
                           , token
                           , addUTCTime (fromIntegral $ C.expires_in token - 5 * 60)
                                        now
                           )
  (a, _) <- runReddit reddit (Throttle now 0 rate ua m authInfo)
  return a

printTPS :: MonadReddit m => m ()
printTPS = do
  t <- getTPS
  putStrLn $ tshow (fromRational t :: Float)

-- For testing
redditTime :: MonadReddit m => m ()
redditTime = numTrans 1 $ do
  now <- getCurrentTime
  putStrLn (tshow now)

instance Monad m => Functor (RedditT m) where
    fmap f m = RedditT $ \t -> do
        (a, t') <- runReddit m t
        return (f a, t')
    {-# INLINE fmap #-}

instance (Functor m, Monad m, MonadIO m) => Applicative (RedditT m) where
    pure a = RedditT $ \t -> return (a,t)
    {-# INLINE pure #-}
    (<*>) = ap
    {-# INLINE (<*>) #-}

instance MonadIO m => Monad (RedditT m) where
  return a = RedditT $ \t -> return (a, t)
  {-# INLINE return #-}
  m >>= k = RedditT $ \t -> do
    now <- liftIO getCurrentTime
    let rate = toRational (tps t)
        count = toRational (numRequests t)
        diff = count / rate
        actualDiff = toRational (diffUTCTime now (startTime t))
    if diff < actualDiff -- Have we waited long enough to not surpass limit?
      then do   -- Run request immediately
        (a, t') <- runReddit m t
        runReddit (k a) t'
      else do
        -- Defer until ready
        -- delay microsecond
        liftIO $ threadDelay (floor $ (diff - actualDiff) * 1000000)
        (a, t') <- runReddit m t
        runReddit (k a) t'
  {-# INLINE (>>=) #-}

instance MonadIO m => MonadIO (RedditT m) where
  liftIO m = RedditT $ \t -> do
                    a <- liftIO m
                    return (a,t)
  {-# INLINE liftIO #-}

instance MonadTrans RedditT where
  lift m = RedditT $ \t -> do
              a <- m
              return (a,t)
  {-# INLINE lift #-}

instance (MonadIO m, MonadBase b m) => MonadBase b (RedditT m) where
    liftBase = liftBaseDefault

instance (MonadIO m, MonadCatch m) => MonadCatch (RedditT m) where
  catch m f = RedditT $ \t -> do
                (a, t') <- runReddit m t `catch` \e ->
                           runReddit (f e) t
                return (a, t')

instance MonadTransControl RedditT where
  type StT RedditT a = (a,Throttle)
  liftWith f = RedditT $ \s ->
                     fmap (\x -> (x,s))
                     (f $ \t -> runReddit t s)
  restoreT = RedditT . const

instance (MonadIO m, MonadThrow m) => MonadThrow (RedditT m) where
  throwM = lift . throwM

instance (MonadIO m, MonadBaseControl b m) => MonadBaseControl b (RedditT m) where
  type StM (RedditT m) a = ComposeSt RedditT m a
  liftBaseWith = defaultLiftBaseWith
  restoreM = defaultRestoreM

getSubreddit :: MonadReddit m => Text -> m (Either String S.Subreddit)
getSubreddit subName = do
  ua <- getUserAgent
  m <- getManager
  mToken <- getToken
  numTrans 1 $ C.getSubreddit m mToken ua subName

getSubreddits :: MonadReddit m
              => String
              -> Maybe Int
              -> Maybe Text
              -> Maybe Text
              -> m (Either String [S.Subreddit])
getSubreddits sorting limit before after = do
  ua <- getUserAgent
  m <- getManager
  mToken <- getToken
  numTrans 1 $ C.getSubreddits m mToken ua sorting limit before after

getLinks :: MonadReddit m => Text -> C.Sort -> Maybe Int -> m (Either String [L.Link])
getLinks subreddit s limit = do
  ua <- getUserAgent
  m <- getManager
  mToken <- getToken
  numTrans 1 $ C.getLinks m mToken ua subreddit s limit

getCommentTree :: MonadReddit m
               => Text
               -> C.Sort
               -> Maybe Int
               -> m (Either String CommentTree)
getCommentTree article s limit = do
  ua <- getUserAgent
  m <- getManager
  mToken <- getToken
  numTrans 1 $ C.getCommentTree m mToken ua article s limit

getMoreChildren :: MonadReddit m => Text -> [Text] -> m (Either String [C.Comment])
getMoreChildren linkId children = do
  ua <- getUserAgent
  m <- getManager
  mToken <- getToken
  numTrans (fromIntegral $ (length children `div` 20) + 1) $
      C.getMoreChildren m mToken ua linkId children

