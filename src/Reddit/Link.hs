{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Reddit.Link
  ( Link (..)
  , Links (..)
  ) where

import ClassyPrelude
import Data.Aeson
import Data.Aeson.Types (Parser)

import Reddit.Utility (parseRedditTime)

newtype Links = Links { toLinks :: [Link] } deriving Show

instance FromJSON Links where
    parseJSON (Object top) = do
        o <- top .: "data"
        Links <$> o .: "children"
    parseJSON _ = mzero

data Link = Link
  { author        :: !Text
  , created_utc   :: !UTCTime
  , domain        :: !Text
  , downs         :: !Int
  , edited        :: !(Maybe UTCTime)
  , gilded        :: !Int
  , hidden        :: !Bool
  , id            :: !Text
  , is_self       :: !Bool
  , name          :: !Text
  , num_comments  :: !Int
  , over_18       :: !Bool
  , permalink     :: !Text
  , score         :: !Int
  , selftext      :: !Text
  , selftext_html :: !(Maybe Text)
  , stickied      :: !Bool
  , subreddit     :: !Text
  , subreddit_id  :: !Text
  , thumbnail     :: !Text
  , title         :: !Text
  , ups           :: !Int
  , url           :: !Text
  } deriving Show

instance FromJSON Link where
    parseJSON (Object o) = do
        lData <- o .: "data" :: Parser Object
        Link
            <$> lData .: "author"
            <*> (lData .: "created_utc" >>= parseRedditTime)
            <*> lData .: "domain"
            <*> lData .: "downs"
            <*> ((Just <$> (lData .: "edited" >>= parseRedditTime)) <|> return Nothing)
            <*> lData .: "gilded"
            <*> lData .: "hidden"
            <*> lData .: "id"
            <*> lData .: "is_self"
            <*> lData .: "name"
            <*> lData .: "num_comments"
            <*> lData .: "over_18"
            <*> lData .: "permalink"
            <*> lData .: "score"
            <*> lData .: "selftext"
            <*> lData .: "selftext_html"
            <*> lData .: "stickied"
            <*> lData .: "subreddit"
            <*> lData .: "subreddit_id"
            <*> lData .: "thumbnail"
            <*> lData .: "title"
            <*> lData .: "ups"
            -- <*> lData .: "upvote_ratio"
            <*> lData .: "url"
    parseJSON _ = error "Not object"

