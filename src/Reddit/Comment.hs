{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}
module Reddit.Comment
  ( MoreComments (..)
  , Comment (..)
  , FC (..)
  , MR (..)
  , commentDepth
  ) where

import ClassyPrelude
import Data.Aeson
import Data.Aeson.Types (Parser)

import Reddit.Utility (parseRedditTime)

newtype MoreComments = MoreComments { toComments :: [ Comment] } deriving Show

instance FromJSON MoreComments where
  parseJSON (Object o) = do
      j <- o .: "json"
      d <- j .: "data"
      MoreComments <$> d .: "things"
  parseJSON _ = mzero

data Comment =
    FullComment !FC
  | More !MR  deriving Show

data FC = FC
  { author            :: !Text
  , body              :: !Text
  , body_html         :: !Text
  , controversiality  :: !Float
  , created_utc       :: !UTCTime
  , downs             :: !Int
  , edited            :: !(Maybe UTCTime)
  , gilded            :: !Int
  , id                :: !Text
  , link_id           :: !Text
  , name              :: !Text
  , parent_id         :: !Text
  , replies           :: ![Comment]
  , score             :: !Int
  , score_hidden      :: !Bool
  , subreddit         :: !Text
  , subreddit_id      :: !Text
  , ups               :: !Int
  } deriving Show

data MR = MR
  { children      :: ![Text]
  , count         :: !Int
  , id_           :: !Text
  , name_         :: !Text
  , parent_id_    :: !Text
  } deriving Show

instance FromJSON Comment where
    parseJSON (Object o) = do
        d <- o .: "data" :: Parser Object
        k <- o .: "kind" :: Parser Text
        if k == "more"
          then More <$> (MR
                    <$> d .: "children"
                    <*> d .: "count"
                    <*> d .: "id"
                    <*> d .: "name"
                    <*> d .: "parent_id"
                    )
          else
            FullComment <$>
                (FC
                <$> d .: "author"
                <*> d .: "body"
                <*> d .: "body_html"
                <*> d .: "controversiality"
                <*> (d .: "created_utc" >>= parseRedditTime)
                <*> d .: "downs"
                <*> ((Just <$> (d .: "edited" >>= parseRedditTime)) <|> return Nothing)
                <*> d .: "gilded"
                <*> d .: "id"
                <*> d .: "link_id"
                <*> d .: "name"
                <*> d .: "parent_id"
                <*> ( do
                    reps <- d .: "replies"
                    case reps of
                        (Object repObject) -> do
                            repData <- repObject .: "data"
                            repData .: "children"
                        _ -> return []
                    )
                <*> d .: "score"
                <*> d .: "score_hidden"
                <*> d .: "subreddit"
                <*> d .: "subreddit_id"
                <*> d .: "ups"
                )
    parseJSON _ = mzero

commentDepth :: Comment -> Int
commentDepth (More _) = 0
commentDepth (FullComment FC{..})
  | null replies      = 0
  | otherwise         = 1 + maximumEx (map commentDepth replies)



