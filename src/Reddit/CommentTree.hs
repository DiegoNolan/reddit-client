{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Reddit.CommentTree
  ( CommentTree (..)
  ) where

import ClassyPrelude
import Data.Aeson
import Data.Vector ((!), (!?))

import Reddit.Comment
import Reddit.Link

data CommentTree = CommentTree
  { link      :: !Link
  , comments  :: ![Comment]
  } deriving Show

instance FromJSON CommentTree where
    parseJSON (Array v) =
        case v !? 1 of
            Nothing -> mzero
            Just cs ->
                CommentTree
                    <$> (case v ! 0 of
                            Object o -> do
                                d <- o .: "data"
                                c <- d .: "children"
                                withArray "linkVector" ( \arr ->
                                    case length arr of
                                        1 -> parseJSON (arr ! 0)
                                        _ -> mzero
                                    ) c
                            _ -> mzero
                        )
                    <*> (case cs of
                            Object o -> do
                                d <- o .: "data" 
                                d .: "children"
                            _ -> mzero
                        )
    parseJSON _ = mzero

