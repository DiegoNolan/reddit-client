let
  pkgs = import <nixpkgs> { };
in
  { reddit-client = pkgs.haskellPackages.callPackage ./default.nix {};
  }
